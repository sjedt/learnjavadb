
# Read Me - Learn Java Database

We'll learn how to connect to an SQL database in Java language. In our example we'll use H2 Database as the main database.
 
What I expected is to show you how to use this technologies.
 
* Set up IntelliJ IDEA IDE, Gradle project management and Git using GitLab.com
* Learn about JDBC Driver
* Learn basic of H2 Database
* Learn plain vanilla JDBC connection with simple Java application to H2
* Move to JPA, may be based on EclipseLink or Hibernate
* Migrate this layer to Spring Boot application and create RESTful Web Services
* Digging deeper in CRUD operations
* Paging, Parameters, PreparedStatement
* DB Transaction
* Binary data
* Cache and performance monitoring

You should follow the tutorial by browsing the source code with tags and download + compare source in each steps.

# Who is maintaining this project?
Worajedt Sitthidumrong \<[sjedt@3ddaily.com][1]\> ... no blog, no website yet. Sorry.

[1]:	mailto:sjedt@3ddaily.com